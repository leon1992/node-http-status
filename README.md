HTTP Status Codes
=================

Package to easily manage HTTP status codes


Example
-------


```javascript
import { HttpStatus, HttpStatusMessage } from '@lleon/http-status';

console.log(HttpStatus.NotFound); // 404
console.log(HttpStatusMessage[HttpStatus.NotFound]) // "Not Found"
console.log(HttpStatusMessage[404]) // "Not Found"
```
