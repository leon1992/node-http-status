import { expect } from 'chai';

import { HttpStatus } from '../src';

describe('HttpStatus', function() {
  describe('Continue', function() {
    it('should have the status code 100', async function() {
      expect(HttpStatus.Continue).to.be.equals(100);
    });
  });

  describe('Switching Protocols', function() {
    it('should have the status code 101', async function() {
      expect(HttpStatus.SwitchingProtocols).to.be.equals(101);
    });
  });

  describe('Processing', function() {
    it('should have the status code 102', async function() {
      expect(HttpStatus.Processing).to.be.equals(102);
    });
  });

  describe('Ok', function() {
    it('should have the status code 200', async function() {
      expect(HttpStatus.Ok).to.be.equals(200);
    });
  });

  describe('Created', function() {
    it('should have the status code 201', async function() {
      expect(HttpStatus.Created).to.be.equals(201);
    });
  });

  describe('Accepted', function() {
    it('should have the status code 202', async function() {
      expect(HttpStatus.Accepted).to.be.equals(202);
    });
  });

  describe('NonAuthoritativeInformation', function() {
    it('should have the status code 203', async function() {
      expect(HttpStatus.NonAuthoritativeInformation).to.be.equals(203);
    });
  });

  describe('NoContent', function() {
    it('should have the status code 204', async function() {
      expect(HttpStatus.NoContent).to.be.equals(204);
    });
  });

  describe('ResetContent', function() {
    it('should have the status code 205', async function() {
      expect(HttpStatus.ResetContent).to.be.equals(205);
    });
  });

  describe('PartialContent', function() {
    it('should have the status code 206', async function() {
      expect(HttpStatus.PartialContent).to.be.equals(206);
    });
  });

  describe('MultiStatus', function() {
    it('should have the status code 207', async function() {
      expect(HttpStatus.MultiStatus).to.be.equals(207);
    });
  });

  describe('AlreadyReported', function() {
    it('should have the status code 208', async function() {
      expect(HttpStatus.AlreadyReported).to.be.equals(208);
    });
  });

  describe('ImUsed', function() {
    it('should have the status code 226', async function() {
      expect(HttpStatus.ImUsed).to.be.equals(226);
    });
  });

  describe('MultipleChoices', function() {
    it('should have the status code 300', async function() {
      expect(HttpStatus.MultipleChoices).to.be.equals(300);
    });
  });

  describe('MovedPermanently', function() {
    it('should have the status code 301', async function() {
      expect(HttpStatus.MovedPermanently).to.be.equals(301);
    });
  });

  describe('Found', function() {
    it('should have the status code 302', async function() {
      expect(HttpStatus.Found).to.be.equals(302);
    });
  });

  describe('SeeOther', function() {
    it('should have the status code 303', async function() {
      expect(HttpStatus.SeeOther).to.be.equals(303);
    });
  });

  describe('NotModified', function() {
    it('should have the status code 304', async function() {
      expect(HttpStatus.NotModified).to.be.equals(304);
    });
  });

  describe('UseProxy', function() {
    it('should have the status code 305', async function() {
      expect(HttpStatus.UseProxy).to.be.equals(305);
    });
  });

  describe('TemporaryRedirect', function() {
    it('should have the status code 307', async function() {
      expect(HttpStatus.TemporaryRedirect).to.be.equals(307);
    });
  });

  describe('PermanentRedirect', function() {
    it('should have the status code 308', async function() {
      expect(HttpStatus.PermanentRedirect).to.be.equals(308);
    });
  });

  describe('BadRequest', function() {
    it('should have the status code 400', async function() {
      expect(HttpStatus.BadRequest).to.be.equals(400);
    });
  });

  describe('Unauthorized', function() {
    it('should have the status code 401', async function() {
      expect(HttpStatus.Unauthorized).to.be.equals(401);
    });
  });

  describe('PaymentRequired', function() {
    it('should have the status code 402', async function() {
      expect(HttpStatus.PaymentRequired).to.be.equals(402);
    });
  });

  describe('Forbidden', function() {
    it('should have the status code 403', async function() {
      expect(HttpStatus.Forbidden).to.be.equals(403);
    });
  });

  describe('NotFound', function() {
    it('should have the status code 404', async function() {
      expect(HttpStatus.NotFound).to.be.equals(404);
    });
  });

  describe('MethodNotAllowed', function() {
    it('should have the status code 405', async function() {
      expect(HttpStatus.MethodNotAllowed).to.be.equals(405);
    });
  });

  describe('NotAcceptable', function() {
    it('should have the status code 406', async function() {
      expect(HttpStatus.NotAcceptable).to.be.equals(406);
    });
  });

  describe('ProxyAuthenticationRequired', function() {
    it('should have the status code 407', async function() {
      expect(HttpStatus.ProxyAuthenticationRequired).to.be.equals(407);
    });
  });

  describe('RequestTimeout', function() {
    it('should have the status code 408', async function() {
      expect(HttpStatus.RequestTimeout).to.be.equals(408);
    });
  });

  describe('Conflict', function() {
    it('should have the status code 409', async function() {
      expect(HttpStatus.Conflict).to.be.equals(409);
    });
  });

  describe('Gone', function() {
    it('should have the status code 410', async function() {
      expect(HttpStatus.Gone).to.be.equals(410);
    });
  });

  describe('LengthRequired', function() {
    it('should have the status code 411', async function() {
      expect(HttpStatus.LengthRequired).to.be.equals(411);
    });
  });

  describe('PreconditionFailed', function() {
    it('should have the status code 412', async function() {
      expect(HttpStatus.PreconditionFailed).to.be.equals(412);
    });
  });

  describe('PayloadTooLarge', function() {
    it('should have the status code 413', async function() {
      expect(HttpStatus.PayloadTooLarge).to.be.equals(413);
    });
  });

  describe('RequestUriTooLong', function() {
    it('should have the status code 414', async function() {
      expect(HttpStatus.RequestUriTooLong).to.be.equals(414);
    });
  });

  describe('UnsupportedMediaType', function() {
    it('should have the status code 415', async function() {
      expect(HttpStatus.UnsupportedMediaType).to.be.equals(415);
    });
  });

  describe('RequestedRangeNotSatisfiable', function() {
    it('should have the status code 416', async function() {
      expect(HttpStatus.RequestedRangeNotSatisfiable).to.be.equals(416);
    });
  });

  describe('ExpectationFailed', function() {
    it('should have the status code 417', async function() {
      expect(HttpStatus.ExpectationFailed).to.be.equals(417);
    });
  });

  describe('ImATeapot', function() {
    it('should have the status code 418', async function() {
      expect(HttpStatus.ImATeapot).to.be.equals(418);
    });
  });

  describe('MisdirectedRequest', function() {
    it('should have the status code 421', async function() {
      expect(HttpStatus.MisdirectedRequest).to.be.equals(421);
    });
  });

  describe('UnprocessableEntity', function() {
    it('should have the status code 422', async function() {
      expect(HttpStatus.UnprocessableEntity).to.be.equals(422);
    });
  });

  describe('Locked', function() {
    it('should have the status code 423', async function() {
      expect(HttpStatus.Locked).to.be.equals(423);
    });
  });

  describe('FailedDependency', function() {
    it('should have the status code 424', async function() {
      expect(HttpStatus.FailedDependency).to.be.equals(424);
    });
  });

  describe('UpgradeRequired', function() {
    it('should have the status code 426', async function() {
      expect(HttpStatus.UpgradeRequired).to.be.equals(426);
    });
  });

  describe('PreconditionRequired', function() {
    it('should have the status code 428', async function() {
      expect(HttpStatus.PreconditionRequired).to.be.equals(428);
    });
  });

  describe('TooManyRequests', function() {
    it('should have the status code 429', async function() {
      expect(HttpStatus.TooManyRequests).to.be.equals(429);
    });
  });

  describe('RequestHeaderFieldsTooLarge', function() {
    it('should have the status code 431', async function() {
      expect(HttpStatus.RequestHeaderFieldsTooLarge).to.be.equals(431);
    });
  });

  describe('ConnectionClosedWithoutResponse', function() {
    it('should have the status code 444', async function() {
      expect(HttpStatus.ConnectionClosedWithoutResponse).to.be.equals(444);
    });
  });

  describe('UnavailableForLegalReasons', function() {
    it('should have the status code 451', async function() {
      expect(HttpStatus.UnavailableForLegalReasons).to.be.equals(451);
    });
  });

  describe('ClientClosedRequest', function() {
    it('should have the status code 499', async function() {
      expect(HttpStatus.ClientClosedRequest).to.be.equals(499);
    });
  });

  describe('InternalServerError', function() {
    it('should have the status code 500', async function() {
      expect(HttpStatus.InternalServerError).to.be.equals(500);
    });
  });

  describe('NotImplemented', function() {
    it('should have the status code 501', async function() {
      expect(HttpStatus.NotImplemented).to.be.equals(501);
    });
  });

  describe('BadGateway', function() {
    it('should have the status code 502', async function() {
      expect(HttpStatus.BadGateway).to.be.equals(502);
    });
  });

  describe('ServiceUnavailable', function() {
    it('should have the status code 503', async function() {
      expect(HttpStatus.ServiceUnavailable).to.be.equals(503);
    });
  });

  describe('GatewayTimeout', function() {
    it('should have the status code 504', async function() {
      expect(HttpStatus.GatewayTimeout).to.be.equals(504);
    });
  });

  describe('HttpVersionNotSupported', function() {
    it('should have the status code 505', async function() {
      expect(HttpStatus.HttpVersionNotSupported).to.be.equals(505);
    });
  });

  describe('VariantAlsoNegotiates', function() {
    it('should have the status code 506', async function() {
      expect(HttpStatus.VariantAlsoNegotiates).to.be.equals(506);
    });
  });

  describe('InsufficientStorage', function() {
    it('should have the status code 507', async function() {
      expect(HttpStatus.InsufficientStorage).to.be.equals(507);
    });
  });

  describe('LoopDetected', function() {
    it('should have the status code 508', async function() {
      expect(HttpStatus.LoopDetected).to.be.equals(508);
    });
  });

  describe('NotExtended', function() {
    it('should have the status code 510', async function() {
      expect(HttpStatus.NotExtended).to.be.equals(510);
    });
  });

  describe('NetworkAuthenticationRequired', function() {
    it('should have the status code 511', async function() {
      expect(HttpStatus.NetworkAuthenticationRequired).to.be.equals(511);
    });
  });

  describe('NetworkConnectTimeoutError', function() {
    it('should have the status code 599', async function() {
      expect(HttpStatus.NetworkConnectTimeoutError).to.be.equals(599);
    });
  });
});
